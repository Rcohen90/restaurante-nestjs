import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Restaurante } from 'src/restaurante/entities/restaurante.entity';
import { Usuario } from 'src/restaurante/entities/usuario.entity';

@Module({
    imports: [
        TypeOrmModule.forRoot({
            type: "mysql",
            host: "localhost",
            port: 3306,
            username: "root",
            password: "based246",
            database: "restaurante",
            entities: ["dist/**/*.entity{.ts,.js}"],
            synchronize: true
          })
    ]
})
export class DatabaseModule {}
