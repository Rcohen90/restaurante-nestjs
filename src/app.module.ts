import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { DatabaseModule } from './modules/database/database.module';
import { RestauranteController } from './restaurante/controllers/restaurante.controller';
import { RestauranteService } from './restaurante/services/restaurante.service';
import { Restaurante } from './restaurante/entities/restaurante.entity';
import { Usuario } from './restaurante/entities/usuario.entity';

import { TypeOrmModule } from '@nestjs/typeorm';
import { UsuarioController } from './restaurante/controllers/usuario.controller';
import { UsuariosService } from './restaurante/services/usuarios.service';
import { ReseniaService } from './restaurante/services/resenia.service';
import { Resenia } from './restaurante/entities/resenia.entity';
import { ReseniaController } from './restaurante/controllers/resenia.controller';

@Module({
  imports: [
              DatabaseModule,
              TypeOrmModule.forFeature([Restaurante, Usuario, Resenia])
            ],
  controllers: [AppController, RestauranteController, UsuarioController, ReseniaController],
  providers: [AppService, RestauranteService, UsuariosService, ReseniaService, Restaurante, Usuario, Resenia]
})
export class AppModule {}
