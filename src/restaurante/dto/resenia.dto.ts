import { Usuario } from "../entities/usuario.entity";
import { Restaurante } from "../entities/restaurante.entity";

export class ReseniaDto {
    readonly user_name: string;
    readonly rating: number;
    readonly review: string;
    readonly created_at: Date;
    readonly update_at: Date;
    readonly usuario: Usuario;
    readonly restaurante: Restaurante;
}
