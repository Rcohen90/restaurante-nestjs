export class RestauranteDto {
    readonly nombre: string;
    readonly direccion: string;
    readonly tipo: number;
}
