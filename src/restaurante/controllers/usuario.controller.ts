import { Controller, Post, Body, Get, Put, Delete, Res, HttpStatus } from '@nestjs/common';
import { UsuariosService } from '../services/usuarios.service';
import { UsuarioDto } from '../dto/usuario.dto';

@Controller('usuario')
export class UsuarioController {

    constructor(private readonly _usuarioService: UsuariosService) { }

    @Get()
    getAll(@Res() response){
        this._usuarioService.getAll().then( usuariosList => {
            response.status(HttpStatus.OK).json(usuariosList);
        }).catch( res => {
            console.log(res);
            response.status(HttpStatus.FORBIDDEN).json({mensaje: 'Error al obtener usuarios'});
        });
    }

    @Post()
    create(@Body() usuarioDto: UsuarioDto, @Res() response) {
        this._usuarioService.createUsuario(usuarioDto).then( user => {
            response.status(HttpStatus.CREATED).json(user);
        }).catch( () => {
            response.status(HttpStatus.FORBIDDEN).json({mensaje: 'Error al crear usuario'});
        });
    }
}
