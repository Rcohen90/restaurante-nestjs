import { Controller, Post, Body, Get, Put, Delete, Res, HttpStatus, Param, HttpCode } from '@nestjs/common';
import { RestauranteDto } from '../dto/restaurante.dto';
import { RestauranteService } from '../services/restaurante.service';

@Controller('restaurante')
export class RestauranteController {
    
    constructor(private readonly _restaService: RestauranteService ) { }

    @Get()
    getAll(@Res() response){
        this._restaService.getAll().then( restaList => {
            response.status(HttpStatus.OK).json(restaList);
        }).catch( res => {
            console.log(res);
            response.status(HttpStatus.FORBIDDEN).json({mensaje: 'Error al obtener restaurantes'});
        });
    }

    @Post()
    create(@Body() restaDTO: RestauranteDto, @Res() response) {
        this._restaService.createResta(restaDTO).then( resta => {
            response.status(HttpStatus.CREATED).json(resta);
        }).catch( () => {
            response.status(HttpStatus.FORBIDDEN).json({mensaje: 'Error al crear restaurante'});
        });
    }

    @Get(':tipo')
    getAllByTipo(@Res() response, @Param('tipo') tipo){
        this._restaService.getAllByTipo(tipo).then( restaList => {
            response.status(HttpStatus.OK).json(restaList);
        }).catch( res => {
            console.log(res);
            response.status(HttpStatus.FORBIDDEN).json({mensaje: 'Error al obtener restaurantes'});
        });
    }
}