import { Controller, Post, Body, Get, Put, Delete, Res, HttpStatus, Param } from '@nestjs/common';
import { ReseniaService } from '../services/resenia.service';
import { ReseniaDto } from '../dto/resenia.dto';

@Controller('resenia')
export class ReseniaController {

    constructor(private readonly _reseniaService: ReseniaService) { }

    @Get()
    getAll(@Res() response){
        this._reseniaService.getAll().then( reseniasList => {
            response.status(HttpStatus.OK).json(reseniasList);
        }).catch( () => {
            response.status(HttpStatus.FORBIDDEN).json({mensaje: 'Error al obtener resenias'});
        });
    }

    @Post()
    create(@Body() reseniasDto: ReseniaDto, @Res() response) {
        this._reseniaService.createResenia(reseniasDto).then( rese => {
            response.status(HttpStatus.CREATED).json(rese);
        }).catch( () => {
            response.status(HttpStatus.FORBIDDEN).json({mensaje: 'Error al crear resenia'});
        });
    }

    @Get(':rating')
    getAllByRating(@Res() response, @Param('rating') rating) {
        this._reseniaService.getAllByRaiting(rating).then( reseniasList => {
            response.status(HttpStatus.OK).json(reseniasList);
        }).catch( () => {
            response.status(HttpStatus.FORBIDDEN).json({mensaje: 'Error al obtener resenias'});
        });
    }
}
