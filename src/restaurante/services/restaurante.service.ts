import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

import { Restaurante } from '../entities/restaurante.entity';
import { RestauranteDto } from '../dto/restaurante.dto';

@Injectable()
export class RestauranteService {
    constructor(
        @InjectRepository(Restaurante)
        private restaRepository: Repository<Restaurante>
      ) {}

      async getAll(): Promise<Restaurante[]>{
        return await this.restaRepository.find();
      }

      async createResta(restaNuevo: RestauranteDto): Promise<Restaurante>{
          const nuevoResta= new Restaurante();
          nuevoResta.nombre = restaNuevo.nombre;
          nuevoResta.direccion = restaNuevo.direccion;
          nuevoResta.tipo = restaNuevo.tipo;

          await this.restaRepository.insert(nuevoResta)
          return nuevoResta;
      }

      async getAllByTipo(tipo: number): Promise<Restaurante[]>{
        return await this.restaRepository.find({tipo});
      }
}
