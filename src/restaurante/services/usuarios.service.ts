import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

import { Usuario } from '../entities/usuario.entity';
import { UsuarioDto } from '../dto/usuario.dto';

@Injectable()
export class UsuariosService {
    constructor(
        @InjectRepository(Usuario)
        private usuarioRepository: Repository<Usuario>
      ) {}

      async getAll(): Promise<Usuario[]>{
        return await this.usuarioRepository.find();
      }

      async createUsuario(usuarioNuevo: UsuarioDto): Promise<Usuario>{
          const nuevoUsuario = new Usuario();
          nuevoUsuario.nombre = usuarioNuevo.nombre; 
          nuevoUsuario.rol = "U"; 

          await this.usuarioRepository.insert(nuevoUsuario)
          return nuevoUsuario;
      }
      
      async getUserById(idUsuario: number): Promise<Usuario>{
        return await this.usuarioRepository.findOne(idUsuario);
      }
}
