import { Injectable } from '@nestjs/common';
import { Resenia } from '../entities/resenia.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { ReseniaDto } from '../dto/resenia.dto';
import { UsuariosService } from './usuarios.service';

@Injectable()
export class ReseniaService {
    constructor(
        @InjectRepository(Resenia)
        private reseniaRepository: Repository<Resenia>,
        private _userService: UsuariosService
      ) {}

      async getAll(): Promise<Resenia[]>{
        return await this.reseniaRepository.find();
      }

      async createResenia(reseniaNuevo: ReseniaDto): Promise<Resenia>{
          const nuevaResenia= new Resenia();
          nuevaResenia.user_name = reseniaNuevo.user_name;
          nuevaResenia.review = reseniaNuevo.review;
          nuevaResenia.rating = reseniaNuevo.rating;
          nuevaResenia.created_at = new Date();
          nuevaResenia.update_at = new Date();
          nuevaResenia.usuario = reseniaNuevo.usuario;

          let idUsu = Number(nuevaResenia.usuario);
          let usu = await this._userService.getUserById(idUsu);
          
          if(usu.rol === 'U') {
            await this.reseniaRepository.insert(nuevaResenia)
            return nuevaResenia;
          } else {
            return null;
          }
      }

      async getAllByRaiting(rating: number): Promise<Resenia[]>{
        return await this.reseniaRepository.find({rating});
      }

}
