import { Entity, Column, PrimaryGeneratedColumn, OneToMany } from 'typeorm';
import { Resenia } from './resenia.entity';

@Entity()
export class Restaurante {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    nombre: string;
    
    @Column()
    direccion: string;

    @Column()
    tipo: number;

    @OneToMany(type => Resenia, resenia => resenia.restaurante)
    resenias: Resenia[];
}
