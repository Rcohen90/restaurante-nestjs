import { Entity, Column, PrimaryGeneratedColumn, Double, ManyToOne, OneToOne } from 'typeorm';
import { Usuario } from './usuario.entity';
import { Restaurante } from './restaurante.entity';
import { RestauranteService } from '../services/restaurante.service';

@Entity()
export class Resenia {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    user_name: string;
    
    @Column({type: 'double'})
    rating: number;

    @Column()
    review: string;

    @Column()
    created_at: Date;

    @Column()
    update_at: Date;

    @ManyToOne(type => Usuario, usuario => usuario.resenias)
    usuario: Usuario;

    @ManyToOne(type => Restaurante, restaurante => restaurante.resenias)
    restaurante: Restaurante;
}
