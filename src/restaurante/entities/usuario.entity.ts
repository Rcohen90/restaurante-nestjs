import { Entity, Column, PrimaryGeneratedColumn, OneToMany } from 'typeorm';
import { Resenia } from './resenia.entity';

@Entity()
export class Usuario {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    nombre: string;
    
    @Column({type: 'char'})
    rol: string;

    @OneToMany(type => Resenia, resenia => resenia.usuario)
    resenias: Resenia[];
}
